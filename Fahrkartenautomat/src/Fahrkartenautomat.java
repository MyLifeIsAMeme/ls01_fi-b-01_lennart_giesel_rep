﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       boolean erneut = true;
       double zuZahlenderBetrag;
       double rückgabebetrag;
       
       while(erneut) {
       //errechnung und Erfassung des Preises Kartenmenge * Kartenpreis
       zuZahlenderBetrag = fahrkartenbestellungErfassen();
       //Bezahlung der Fahrkarten
       rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
       //Fahrkarten Ausgabe
       fahrkartenAusgeben();
       //Rückgeldauszahlung
       rueckgeldAusgeben(rückgabebetrag);
       erneut = naechster();
       }
    }
    
    //Erfassung und errechnung des Fahrkartenpreises durch menge und Preis
	public static double fahrkartenbestellungErfassen() {
		Scanner tastatur = new Scanner(System.in);
		
		double kartenpreis = 0;
		double mengeKarten = 0;
		int kartentyp = 0;
		boolean weiter = true;
		
		System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:\n"
    			+ "  (1) Einzelfahrschein Regeltarif AB [2,90 EUR]\n"
    			+ "  (2) Tageskarte Regeltarif AB [8,60 EUR]\n"
    			+ "  (3) Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR]\n");
	    kartentyp = tastatur.nextInt();
	    while(weiter) {
	    switch (kartentyp) {
	    	case 1:
	    		kartenpreis = 2.90;
	    		weiter = false;
	    		break;
	    	case 2:
	    		kartenpreis = 8.60;
	    		weiter = false;
	    		break;
	    	case 3:
	    		kartenpreis = 23.50;
	    		weiter = false;
	    		break;
	    	default:
	    		System.out.println("Sie haben eine Ungültige wahl getroffen. Bitte wählen nur sie zwischen 1, 2 und 3.");
	    		weiter = true;
	    		break;
	    } 
	    }
	    
	    System.out.print("Wie viele Karten möchten sie haben? \n");
	    System.out.print("Bitte wählen sie einen Betrag von 1 bis 10.");
	    mengeKarten = tastatur.nextDouble();
	    
	    if(mengeKarten<1||mengeKarten>10){
	    	System.out.println("Es wurde gesagt bitte nur einen Wert von 1 bis 10. Wir haben die Bestellmenge auf 1 gesetzt.");
	    	mengeKarten = 1;
	    }
	    
	    double zuZahlenderBetrag = mengeKarten*kartenpreis;
	    return zuZahlenderBetrag;
    }
	//Bezahlung der Fahrkarten
	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		
		Scanner tastatur = new Scanner(System.in);
		double eingezahlterGesamtbetrag;
	    double eingeworfeneMünze;
	    double rückgabebetrag;
		eingezahlterGesamtbetrag = 0.0;
	       
	    while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
	    {
	    	double nochZuZahlenderBetrag = zuZahlenderBetrag - eingezahlterGesamtbetrag;
	     	System.out.printf("Noch zu zahlen: %1.2f Euro\n", nochZuZahlenderBetrag);
	     	System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
	     	eingeworfeneMünze = tastatur.nextDouble();
	     	eingezahlterGesamtbetrag += eingeworfeneMünze;
	    }
		
	    rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
	    
		return rückgabebetrag;
	}
	//Druckzeit Simmulation
	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	       }
	       System.out.println("\n\n");
	}
	//Rückgeldauszahlung
	public static void rueckgeldAusgeben(double rückgabebetrag) {
		// Rückgeldberechnung und -Ausgabe
	       // -------------------------------
	      // rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
	       rückgabebetrag = Math.round(rückgabebetrag*100)/100.0;
	       if(rückgabebetrag > 0.0)
	       {
	    	   System.out.printf("Der Rückgabebetrag in Höhe von %1.2f Euro ",rückgabebetrag);
	    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

	           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
	           {
	        	  System.out.println("2 EURO");
		          rückgabebetrag -= 2.0;
		          rückgabebetrag = Math.round(rückgabebetrag*100)/100.0;
	           }
	           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
	           {
	        	  System.out.println("1 EURO");
		          rückgabebetrag -= 1.0;
		          rückgabebetrag = Math.round(rückgabebetrag*100)/100.0;
	           }
	           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
	           {
	        	  System.out.println("50 CENT");
		          rückgabebetrag -= 0.5;
		          rückgabebetrag = Math.round(rückgabebetrag*100)/100.0;
	           }
	           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
	           {
	        	  System.out.println("20 CENT");
	 	          rückgabebetrag -= 0.2;
	 	          rückgabebetrag = Math.round(rückgabebetrag*100)/100.0;
	           }
	           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
	           {
	        	  System.out.println("10 CENT");
		          rückgabebetrag -= 0.1;
		          rückgabebetrag = Math.round(rückgabebetrag*100)/100.0;
		          
	           }
	           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
	           {
	        	  System.out.println("5 CENT");
	 	          rückgabebetrag -= 0.05;
	 	          rückgabebetrag = Math.round(rückgabebetrag*100)/100.0; 
	           }
	       }

	       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
	                          "vor Fahrtantritt entwerten zu lassen!\n"+
	                          "Wir wünschen Ihnen eine gute Fahrt.");
	}
	
	public static boolean naechster() {
		
		Scanner tastatur = new Scanner(System.in);
		boolean erneut = true;
		boolean weiter = true;
		int eingabe = 0;
		
		System.out.println("Soll eine weitere Bestellung getätigt werden? \n"+
		"1 = Ja, 2 = Nein");
		eingabe = tastatur.nextInt();
		while (weiter) {
		switch(eingabe) {
		case 1:
			System.out.println("Okay wir machen sofort weiter.");
			weiter = false;
			erneut = true;
			break;
		case 2:
			System.out.println("Okay wir werden den Automat jetzt beenden.");
			weiter = false;
			erneut = false;
			break;
		default:
			System.out.println("Die Eingabe war falsch bitte wählen sie nur 1 oder 2.");
			weiter = true;
			break;
		}
		}
		return erneut; 
	}
}
	


public class Placeholder {

	public static void main(String[] args) {
		
			/**s% verursacht einen Platzhalter welcher sich nach "sx%" berechnet
			  das "s" steht hier f�r String und kann auch mit "d" f�r dezimal gewechselt werden
			  sowie auch "f" f�r float, kann alles auch mehrfach verwendet werden*/
		
		System.out.printf("Hello %5s lulululul %s \n", "oh", "meh");
		
		System.out.printf("Hello %3d\n", 200);
		
		System.out.printf("Hello %13f\n", 300.00);
		
		//"System.out.printf" is used to print formatted strings using various format specifiers.
		
	}

}

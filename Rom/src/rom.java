import java.util.Scanner;
import java.util.ArrayList;

public class rom {

	public static void main(String[] args) {
		
		//ASCII;73 I = 1, ASCII;86 V = 5, ASCII;88 X = 10, ASCII;76 L = 50, ASCII;67 C = 100, ASCII;68 D = 500, ASCII;77 M = 1000
		String zahlRoemisch = "";
		int zahlNormal = 0;
		System.out.println("Welche R�mischezahl m�chten sie in Dezimal erhalten");
		Scanner scanner = new Scanner(System.in);
		zahlRoemisch = scanner.next();
		zahlNormal = entRoemisieren(zahlRoemisch);
		System.out.println(zahlNormal);
	}
	
	public static int entRoemisieren(String eingabe) {
		
		int speicher = 0;
		int zwischen1 = 0;
		int zwischen2 = 0;
		//Wandelt den String eingabe in einen Char Array um
		char[] ch = new char[eingabe.length()]; 
		for (int i = 0; i < eingabe.length(); i++) { 
            ch[i] = eingabe.charAt(i); 
        } 
		//checkt welches ASCII Zeichen die jeweilige Stelle ist und f�gt den Wert zu speicher zu
		for (int i=0 ; i<ch.length; i++ ) {
			switch (ch[i]) {
			case 77: 
				speicher += 1000;
				zwischen1 = 1000;
				break;
			case 68:
				speicher += 500;
				zwischen1 = 500;
				break;
			case 67:
				speicher += 100;
				zwischen1 = 100;
				break;
			case 76:
				speicher += 50;
				zwischen1 = 50;
				break;
			case 88:
				speicher += 10;
				zwischen1 = 10;
				break;
			case 86:
				speicher += 5;
				zwischen1 = 5;
				break;	
			case 73:
				speicher += 1;
				zwischen1 = 1;
				break;	
			}
			//�berpr�ft die vorgehende Zahl und speichert sie in zwischen2
			if (i>=1) {
			switch (ch[i-1]) {
			case 77: 
				
				zwischen2 = 1000;
				break;
			case 68:
				
				zwischen2 = 500;
				break;
			case 67:
				
				zwischen2 = 100;
				break;
			case 76:
				
				zwischen2 = 50;
				break;
			case 88:
				
				zwischen2 = 10;
				break;
			case 86:
				
				zwischen2 = 5;
				break;	
			case 73:
				
				zwischen2 = 1;
				break;	
			}
			if (zwischen2<zwischen1) {
				speicher = speicher - (zwischen2*2);
			}
		}
				
		}
		return speicher;
	}
}
